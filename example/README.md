# FAPESP Example Project

You can copy paste this project folder into your working directory. Ideally that directory should be outside of your local texmf tree, but there is no restriction about it.

This example includes:
- `project.tex`: the barebones of a project to showcase a standard project.
- `project.bib`: the barebones of a bibliography that goes with `project.tex`.
- `.gitlab-ci.yml`: an example of a pipeline to build projects using this template and its dependencies automatically. 