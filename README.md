# About

This style formats an article to add cover pages for formating a research project.

It provides an easy template to follow and it creates title pages in Portuguese and English.

In particular, it follows rules that allows you to format your project with FAPESP standards.

# Install

## Clone

The repository should be cloned into your local `texmf` tree.

- In Unix systems:
```bash
mkdir -p ~/texmf/tex/latex/
cd ~/texmf/tex/latex/
git clone git@gitlab.com:adin/project-template.git
```

- In macOS system (when latex is installed with mactex):
```bash
mkdir -p ~/Library/texmf/tex/latex/
cd ~/Library/texmf/tex/latex/
git clone git@gitlab.com:adin/project-template.git
```

- In other operating systems it may vary depending on your installation. To discover the path you can
```bash
kpsewhich -var-value TEXMFHOME
```

## Download

Another option is to download the files and paste them in your local `texmf` tree.

Create a directory to store the package. In our case we will create `project-template`

In Unix systems:
```bash
mkdir -p ~/texmf/tex/latex/project-template
```

In macOS system (when latex is installed with mactex):
```bash
mkdir -p ~/Library/texmf/tex/latex/project-template
```

Download the file `project.sty`, and paste it in the previous folder.

# Use

Include the package in your main document with

```latex
\usepackage{project}
```

For a more detail example, see [here](example/project.tex).